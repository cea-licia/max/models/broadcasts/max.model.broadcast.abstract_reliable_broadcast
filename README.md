# max.model.broadcast.abstract_reliable_broadcast

A simple abstraction for reliable broadcast algorithms.

It provides:
- a generic handler for the start of the broadcast procedure (sometimes called "r_bcast" in the literature)
- a generic handler for the finalization of the broadcast procedure (sometimes called "r_deliver" in the literature)

It defines:
- a parameterizable network of "n" broadcast peers 
- a primitive simple broadcast action that corresponds to :
  - a specific peer broadcasting a specific message payload such that..
  - .. all the other "n-1" broadcast peers in the network will potentially receive it 
  (up to the delays and loss defined w.r.t. the underlying [max.model.network.stochastic_adversarial_p2p](https://gitlab.com/cea-licia/max/models/networks/max.model.network.stochastic_adversarial_p2p) model)..
  - .. the peer itself immediately receives the message


## Models

We have the following models that implement this present reliable broadcast interface:
- [max.model.broadcast.trivial_broadcast](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.trivial_broadcast)
- [max.model.broadcast.bracha_brb](https://gitlab.com/cea-licia/max/models/broadcasts/max.model.broadcast.bracha_brb)

# Licence

__max.model.broadcast.abstract_reliable_broadcast__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)













