/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.env;

import max.core.Context;
import max.core.agent.SimulatedAgent;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;

import java.util.logging.Level;

/**
 * An environment for reliable broadcasts.
 *
 * @author Erwan Mahe
 */
public class AbstractBroadcastEnvironment<T_msg, A extends BroadcastPeerAgent> extends StochasticP2PEnvironment {

    /**
     * The name of the environment in which the final delivery of broadcast messages are to be handled.
     * **/
    public final String messageDeliveryEnvironment;

    public AbstractBroadcastEnvironment(String messageDeliveryEnvironment) {
        super();
        getLogger().setLevel(Level.INFO);
        addAllowedRole(RBroadcastPeer.class);
        this.messageDeliveryEnvironment = messageDeliveryEnvironment;
    }

    @Override
    protected Context createContext(SimulatedAgent agent) {
        return new BroadcastPeerContext<T_msg,A>((BroadcastPeerAgent) agent, this);
    }

}
