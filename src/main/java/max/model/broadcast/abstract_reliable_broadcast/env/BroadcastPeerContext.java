/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.env;

import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.env.StochasticP2PEnvironment;



/**
 * Context for peers in a broadcast environment.
 *
 * @author Erwan Mahe
 */
public class BroadcastPeerContext<T_msg,A extends BroadcastPeerAgent> extends StochasticP2PContext {

    /**
     * Handler for r_bcast
     * **/
    public ReliableBroadcastHandler<T_msg,A> startHandler;

    /**
     * Handler for r_deliver
     * **/
    public ReliableBroadcastHandler<T_msg,A> finalHandler;


    public BroadcastPeerContext(BroadcastPeerAgent owner, StochasticP2PEnvironment environment) {
        super(owner, environment);
    }

}
