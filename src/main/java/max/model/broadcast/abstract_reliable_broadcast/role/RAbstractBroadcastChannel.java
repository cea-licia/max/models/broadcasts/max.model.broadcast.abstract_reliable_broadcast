/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.role;


import max.model.network.stochastic_adversarial_p2p.role.RStochasticP2PEnvironment;

/**
 * Role taken by the broadcast environment.
 *
 * @author Erwan Mahe
 */
public interface RAbstractBroadcastChannel extends RStochasticP2PEnvironment {}
