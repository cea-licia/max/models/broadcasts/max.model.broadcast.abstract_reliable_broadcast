/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.behavior;


import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;

/**
 * Handler for reliable broadcast.
 * Can be used to:
 * - handle the start of the broadcast
 * - handle the finalization of the broadcast
 *
 * @author Erwan Mahe
 */
@FunctionalInterface
public interface ReliableBroadcastHandler<T_msg, A extends BroadcastPeerAgent> {

    void handle(T_msg message, A owner, String relevantEnvironment);

}
