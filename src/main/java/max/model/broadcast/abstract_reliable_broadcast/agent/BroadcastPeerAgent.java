/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.agent;

import max.core.action.Plan;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.network.stochastic_adversarial_p2p.agent.StochasticPeerAgent;

/**
 * Base class for Broadcast Peer agents.
 *
 * @author Erwan Mahe
 */
public class BroadcastPeerAgent extends StochasticPeerAgent {


    public BroadcastPeerAgent(Plan<? extends BroadcastPeerAgent> plan) {
        super(plan);
        addPlayableRole(RBroadcastPeer.class);
    }

}
