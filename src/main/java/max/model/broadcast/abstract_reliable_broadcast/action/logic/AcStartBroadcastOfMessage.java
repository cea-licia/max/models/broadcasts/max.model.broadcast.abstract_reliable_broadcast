/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.action.logic;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.env.BroadcastPeerContext;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;


/**
 * Action executed when a peer initializes a broadcast.
 *
 * @author Erwan Mahe
 */
public class AcStartBroadcastOfMessage<T_msg, A extends BroadcastPeerAgent> extends Action<A> {

    private final T_msg message;

    public AcStartBroadcastOfMessage(String environmentName, A owner, T_msg message) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.message = message;
    }

    @Override
    public void execute() {
        BroadcastPeerContext<T_msg,A> context = (BroadcastPeerContext<T_msg,A>) this.getOwner().getContext(this.getEnvironment());
        context.startHandler.handle(this.message, this.getOwner(), this.getEnvironment());
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcStartBroadcastOfMessage(getEnvironment(), getOwner(), this.message);
    }
}
