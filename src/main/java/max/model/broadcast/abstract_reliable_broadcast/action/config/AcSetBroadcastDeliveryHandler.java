/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.action.config;

import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.behavior.ReliableBroadcastHandler;
import max.model.broadcast.abstract_reliable_broadcast.env.BroadcastPeerContext;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;


/**
 * Action executed when (re)setting the delivery handler for the reliable broadcast.
 *
 * @author Erwan Mahe
 */
public class AcSetBroadcastDeliveryHandler<T_msg, A extends BroadcastPeerAgent> extends Action<A> {

    private final ReliableBroadcastHandler<T_msg, A> deliveryHandler;

    public AcSetBroadcastDeliveryHandler(String environmentName, A owner, ReliableBroadcastHandler<T_msg, A> deliveryHandler) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.deliveryHandler = deliveryHandler;
    }

    @Override
    public void execute() {
        BroadcastPeerContext<T_msg, A> context = (BroadcastPeerContext<T_msg, A>) this.getOwner().getContext(this.getEnvironment());
        context.finalHandler = this.deliveryHandler;
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcSetBroadcastDeliveryHandler(getEnvironment(), getOwner(), this.deliveryHandler);
    }
}
