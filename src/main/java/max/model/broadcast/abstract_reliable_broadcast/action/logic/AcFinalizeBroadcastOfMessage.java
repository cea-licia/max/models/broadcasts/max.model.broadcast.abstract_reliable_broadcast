/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.action.logic;


import max.core.action.Action;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.env.AbstractBroadcastEnvironment;
import max.model.broadcast.abstract_reliable_broadcast.env.BroadcastPeerContext;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;


/**
 * Action executed when a peer finalizes a broadcast.
 *
 * @author Erwan Mahe
 */
public class AcFinalizeBroadcastOfMessage<T_msg, A extends BroadcastPeerAgent> extends Action<A> {

    private final T_msg message;

    public AcFinalizeBroadcastOfMessage(String environmentName, A owner, T_msg message) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.message = message;
    }

    @Override
    public void execute() {
        BroadcastPeerContext<T_msg,A> context = (BroadcastPeerContext<T_msg,A>) this.getOwner().getContext(this.getEnvironment());
        AbstractBroadcastEnvironment env = (AbstractBroadcastEnvironment) context.getEnvironment();
        context.finalHandler.handle(this.message, this.getOwner(),env.messageDeliveryEnvironment);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new AcFinalizeBroadcastOfMessage(getEnvironment(), getOwner(), this.message);
    }
}
