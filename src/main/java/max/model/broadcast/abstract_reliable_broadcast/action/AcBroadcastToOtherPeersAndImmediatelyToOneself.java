/*********************************************************************
 * Copyright (c) 2023 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/

package max.model.broadcast.abstract_reliable_broadcast.action;

import madkit.kernel.AgentAddress;
import max.core.action.Action;
import max.datatype.com.Message;
import max.model.broadcast.abstract_reliable_broadcast.agent.BroadcastPeerAgent;
import max.model.broadcast.abstract_reliable_broadcast.role.RBroadcastPeer;
import max.model.network.stochastic_adversarial_p2p.action.message.AcEmitMessageTowardsNetwork;
import max.model.network.stochastic_adversarial_p2p.action.message.AcHandleMessageOnReception;
import max.model.network.stochastic_adversarial_p2p.context.StochasticP2PContext;
import max.model.network.stochastic_adversarial_p2p.role.RStochasticNetworkPeer;

import java.security.SecureRandom;
import java.util.*;

/**
 * Action executed whenever a peer uses a primitive broadcast which corresponds to:
 * - the (n-1) other broadcast peers potentially receiving the message after some delays (as per the P2P model)
 * - itself immediately receiving the message
 *
 * @author Erwan Mahe
 */
public class AcBroadcastToOtherPeersAndImmediatelyToOneself<T_payload,A extends BroadcastPeerAgent> extends Action<A> {

    private final String messageType;
    private final String messageCountKey;

    private final T_payload messagePayload;

    public AcBroadcastToOtherPeersAndImmediatelyToOneself(String environmentName,
                                                          A owner,
                                                          T_payload messagePayload,
                                                          String messageType,
                                                          String messageCountKey) {
        super(environmentName, RBroadcastPeer.class, owner);
        this.messagePayload = messagePayload;
        this.messageType = messageType;
        this.messageCountKey = messageCountKey;
    }

    @Override
    public void execute() {
        getOwner().getLogger().fine("Primitive broadcast on environment " + getEnvironment() + " by peer " + getOwner().getName());
        StochasticP2PContext context = (StochasticP2PContext) getOwner().getContext(getEnvironment());
        AgentAddress myP2PAddress = context.getMyAddress(RStochasticNetworkPeer.class.getName());
        // ***
        Set<String> nodesNames = new HashSet<>();
        List<AgentAddress> nodesAddresses = new ArrayList<>();
        for (AgentAddress agentAddress : getOwner().getAgentsWithRole(getEnvironment(), RBroadcastPeer.class)) {
            A targetAgent = (A) agentAddress.getAgent();
            AgentAddress targetP2Paddress = targetAgent.getAgentAddressIn(getEnvironment(), RStochasticNetworkPeer.class);
            if (!targetP2Paddress.equals(myP2PAddress)) {
                nodesNames.add(targetAgent.getName());
                nodesAddresses.add(targetP2Paddress);
            }
        }
        // shuffle target message recipients addresses to ensure statistical fair access priority
        Collections.shuffle(nodesAddresses, new SecureRandom());
        //
        Message<AgentAddress, T_payload> messageToOthers = new Message<>(myP2PAddress, nodesAddresses, this.messagePayload);
        messageToOthers.setType(this.messageType);
        messageToOthers.setCountKey(this.messageCountKey);
        getOwner().getLogger().fine("Reliable Broadcast Layer : primitive broadcast from " + getOwner().getName() + " to " + nodesNames);
        (new AcEmitMessageTowardsNetwork<A>(getEnvironment(), getOwner(), messageToOthers)).execute();
        // ***
        Message<AgentAddress, T_payload> messageToSelf = new Message<>(myP2PAddress, myP2PAddress, this.messagePayload);
        messageToSelf.setType(this.messageType);
        messageToSelf.setCountKey(this.messageCountKey);
        getOwner().getLogger().fine("Reliable Broadcast Layer : immediate delivery to oneself : " + getOwner().getName());
        (new AcHandleMessageOnReception<A>(getEnvironment(), getOwner(), messageToSelf)).execute();
    }

}
