open module max.model.broadcast.abstract_reliable_broadcast {
    // Exported packages
    exports max.model.broadcast.abstract_reliable_broadcast.action;
    exports max.model.broadcast.abstract_reliable_broadcast.action.config;
    exports max.model.broadcast.abstract_reliable_broadcast.action.logic;
    exports max.model.broadcast.abstract_reliable_broadcast.agent;
    exports max.model.broadcast.abstract_reliable_broadcast.behavior;
    exports max.model.broadcast.abstract_reliable_broadcast.env;
    exports max.model.broadcast.abstract_reliable_broadcast.role;

    // Dependencies
    requires java.logging;
    requires java.desktop;
    requires org.apache.commons.lang3;

    requires madkit;      // Automatic

    requires max.core;
    requires max.datatype.com;
    requires max.model.network.stochastic_adversarial_p2p;

    // Optional dependency (required for tests)
    requires static org.junit.jupiter.api;

}